// Allow us to use the Express frameworks
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use body parser (for reading submitted form data)
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); // Allows us to read forms submitted with POST requests

// This code will redirect all the URLs that end with a "/" to the same URL but without the "/".
// For example, if the user navigates to http://localhost:3000/about/, they will be redirected
// to http://localhost:3000/about. This will make the image URLs and stuff in the HTML files always
// work correctly.
app.use(function (req, res, next) {
    if (req.path.substr(-1) == '/' && req.path.length > 1) {
        var query = req.url.slice(req.path.length);
        res.redirect(301, req.path.slice(0, -1) + query);
    } else {
        next();
    }
});

// Helper function to make sure that the returned result is an array.
function ensureArray(value) {
    if (value == undefined) return [];
    else return Array.isArray(value) ? value : [value];
}

// Specify that when we browse to "/" with a GET request, show the example01/form view
app.get('/', function (req, res) {

    res.render("example01/form");
});

// Specify that when we browse to "/submit" with a POST request, read the submitted form values and then render submitForm.handlebars.
// To process POSTed forms, use req.body
app.post('/submit', function (req, res) {

    console.log(req.body.options);

    // Read the form data
    var data = {
        name: req.body.name,
        options: ensureArray(req.body.options)
    };

    res.render("example01/submitForm", data);
});

// Specify that when we browse to "/submit" with a GET request, read the submitted form values and then render submitForm.handlebars.
// To process GETted forms, use req.query
app.get('/submit', function (req, res) {

    console.log(req.query.options);

    // Read the form data
    var data = {
        name: req.query.name,
        options: ensureArray(req.query.options)
    };

    res.render("example01/submitForm", data);
});

/* We haven't specified any other routes, so browsing anywhere other than "/" will result in a default error page being returned. */

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});